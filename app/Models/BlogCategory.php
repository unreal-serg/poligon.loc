<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * BlogCategory
 *
 * @mixin Eloquent
 */

class BlogCategory extends Model
{
    use SoftDeletes;

    protected $fillable = ['title', 'slug', 'parent_id', 'description'];
}
